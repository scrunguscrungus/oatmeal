# Oatmeal - Psychonauts Scene Converter

## What is Oatmeal?
Besides being a funky lil' guy who helps you traverse mental worlds, **Oatmeal** is a tool for converting *Psychonauts*' file formats. Specifically, converting the game's scene format to and from *glTF*.

## Quick Summary
Oatmeal can convert Psychonauts scene files (*PLB* files) to glTF files, and glTF files to Psychonauts scene files.
Its current level of support is detailed below. Oatmeal is a work in progress.

### Supported Features
#### PLB -> glTF
- Mesh Data
- Some material data
- UV data
- Skeleton data
- Rigging data
- Conversion of animations via *.jan* files
### glTF -> PLB
- Mesh data
- Some material data
- UV data
- Skeleton data
- Rigging data

## Detailed Explanations


### What's a scene?
A *scene* (identified by the *PLB* file extension) in the Psychonauts engine is quite a lot of things. The most basic use it has is storing model data but scene files are also used to store entire levels including information about triggers, entities in those levels, and so on.

Simply put, a scene is a collection of meshes, triggers, entities, collision data and more that Psychonauts can load.

### What's a glTF?
glTF (*GL Transmission Format*) is an open file format standard for the storage of 3D model and scene data. It's a highly modern, versatile format with a lot of neat features and is supported by default in software such as *Blender*, making it a good fit for a converstion target for Psychonauts' scene format.

### How does Oatmeal factor into this?
**Oatmeal** faciliates conversion between Psychonauts' scene files and glTF files. It does so by using [PsychoPortal](https://gitlab.com/scrunguscrungus/psychoportal) to read and write Psychonauts' data, and [SharpGLTF](https://github.com/vpenades/SharpGLTF/) to read and write glTF data.

This makes it possible to import files from Psychonauts into Blender, and to export files from Blender into Psychonauts.

### What are the supported features?
Scene files, especially those used by levels, are complex with a lot of components that go into them. Every data type must be converted from Psychonauts' internal data structures into that of glTF and vice versa. In addition, it takes additional work to make this process work both ways.

As such, Oatmeal currently only supports a limited subset of features of the Psychonauts scene format. Because of the additional work required to convert back to scene files from glTF, the glTF -> Scene process is generally behind and usually supports an even further limited set of features at any given time.

Oatmeal is under active development and over time it will support more parts of the scene format.
