﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json.Nodes;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using PsychoPortal;

using Oatmeal.Constants;

using SharpGLTF;
using System.Numerics;
using SharpGLTF.Scenes;

namespace Oatmeal
{
	public enum LiPoEmptyType
	{
		NONE,
		ENTITY,
		MESH
	}

	internal static class ExtrasUtils
	{
		//BASEBUILDER
		private static JObject? GetExtras(this JsonNode a)
		{
			JObject? extras = JObject.Parse(a.ToJsonString());
			return extras;
		}

		public static JObject? GetLipoData(JsonNode? a)
		{
			JObject? extras = a?.GetExtras();
			if (extras == null )
				return null;

			if (!extras.ContainsKey("lipo_data"))
				return null;

			return extras?.GetValue("lipo_data") as JObject;
		}



		public static T? GetValue<T>(JsonNode? a, string value, T def = default!)
		{
			JObject? ld = GetLipoData(a);
			if (ld == null)
				return def;

			string[] keys = value.Split('.');
			JObject? k = ld;
			string finalKey = value;
			foreach ( string key in keys )
			{
				JToken t = k?.GetValue(key);
				if (t is not JObject)
				{
					finalKey = key;
					continue;
				}	
				else
				{
					k = t as JObject;
				}
				if (k == null)
					return def;
			}

			return k.TryGetValue(finalKey, out JToken tok) ? tok.ToObject<T>() : def;
		}

		public static JsonNode SetValue<T>(JsonNode? a, string value, T val)
		{
			if ( a == null )
			{
				a = JsonNode.Parse("{}");
			}
			JObject? ld = GetLipoData(a);
			if ( ld == null )
			{
				ld = new JObject();
			}

			string[] keys = value.Split('.');
			JObject? k = ld;
			string finalKey = value;
			foreach (string key in keys)
			{
				if (key == keys.Last())
				{
					finalKey = key;
					continue;
				}	

				JToken? t = k?.GetValue(key);
				if ( t == null )
				{
					k[key] = new JObject();
					t = k[key];
				}
				else if ( t is not JObject )
				{
					Console.WriteLine($"Tried to set extras value {value}, but {key} existed and isn't a JObject");
					return a;
				}

				k = t as JObject;
			}


			k[finalKey] = JToken.FromObject(val);
			a["lipo_data"] = JsonNode.Parse(ld.ToString());
			return a;
		}

		public static JsonNode SetVec2(JsonNode? a, string value, Vec2 val)
		{
			a = SetValue(a, $"{value}.X", val.X);
			a = SetValue(a, $"{value}.Y", val.Y);
			return a;
		}


		public static bool IsEntity(JsonNode? a)
		{
			LiPoEmptyType type = GetValue(a, "lipo_type", LiPoEmptyType.NONE);
			return type == LiPoEmptyType.ENTITY;
		}
		public static bool IsMesh(JsonNode? a)
		{
			LiPoEmptyType type = GetValue(a, "lipo_type", LiPoEmptyType.NONE);
			return type == LiPoEmptyType.MESH;
		}
	}
}
