﻿using System.Numerics;

using Newtonsoft.Json;

using PsychoPortal;

using SharpGLTF.Schema2;

namespace Oatmeal
{
	internal class Program
	{
		internal static string inputFilename = "";
		public static PsychonautsSettings settings;

		static void ParsePLBData(Scene scn, AnimToProcess[]? anim = null)
		{
			PLBConverter.ConvertAndSaveDomainMeshes(scn, scn.RootDomain, anim);
		}

		static void Main(string[] args)
		{
			string file;
			string janFile = string.Empty;

#if DEBUG
			if (args.Length < 1)
			{


				//file = "dogen.plb";
				//file = "dogen_Dummy_DomainFirstMesh.gltf";

				//file = "jilltest.gltf";
				//file = "jilltest_bt.gltf";

				//file = "realbeba.gltf";
				//file = "demon.plb";
				//file = "animTest.gltf";

				//file = "demon.plb";
				//janFile = "./demonanims";

				//file = "MMI1.plb";
				//file = "triangle_moment.gltf";
				file = "lightmaptest.gltf";
			}
			else
			{
#endif
				file = args[0].Trim('"');
				if ( args.Length > 1 )
				{
					janFile = args[1].Trim('"');
				}
#if DEBUG
			}
#endif
			settings = args.Contains("PS2") ? new(PsychonautsVersion.PS2) : new(PsychonautsVersion.PC_Digital);
			inputFilename = Path.GetFileNameWithoutExtension(file);

			Console.WriteLine($"Oatmeal 1.5\nFile:{file}\n");

#if DEBUG
			if ( false )
			{
				//PackPack pack = Binary.ReadFromFile<PackPack>(file, settings);
				Scene scn = Binary.ReadFromFile<Scene>(file, settings);
				//Scene scn = pack.Scene;
				Vec3[] verts = scn.RootDomain.Meshes[0].CollisionTree.Vertices;
				CollisionPoly2[] polys = scn.RootDomain.Meshes[0].CollisionTree.CollisionPolys;

				StreamWriter sw = new(File.Open("PolyReport.log", FileMode.Create));

				for (int i = 0; i < polys.Length; i++)
				{
					CollisionPoly2 poly = polys[i];
					Vec3 A = verts[poly.VertexIndices[0]];
					Vec3 B = verts[poly.VertexIndices[1]];
					Vec3 C = verts[poly.VertexIndices[2]];

					sw.WriteLine($"Poly {i}");
					sw.WriteLine($"A: {A}");
					sw.WriteLine($"B: {B}");
					sw.WriteLine($"C: {C}");
					sw.WriteLine($"Factor: {poly.NormalizationFactor} (0x{BitConverter.ToUInt32(BitConverter.GetBytes(poly.NormalizationFactor)):X8})");

					Vec3 BA = B - A;
					Vec3 CA = C - A;
					byte[] bytesA = BitConverter.GetBytes(poly.NormalizationFactor);

					Vector3 Cross = Vector3.Cross(CA.ToVector3(), BA.ToVector3());
					sw.WriteLine($"Cross: {Cross}");
					sw.WriteLine($"Cross Length: {Cross.Length()}");
					Vector3 normal = Cross * poly.NormalizationFactor;
					sw.WriteLine($"Cross*Factor: {normal}");
					sw.WriteLine($"Cross*Factor length: {normal.Length()}");
					sw.WriteLine("...");
					float ours = 1 / Cross.Length();

					byte[] bytesB = BitConverter.GetBytes(ours);
					if ( !bytesA.SequenceEqual(bytesB) )
					{
						Console.WriteLine($"MISMATCH ON POLY {i}");
						Console.ReadKey();
					}
					Vector3 oursResult = Cross * ours;
					sw.WriteLine($"Our calculation (1 / Cross.Length()): {ours} (0x{BitConverter.ToUInt32(BitConverter.GetBytes(ours)):X8})");
					sw.WriteLine($"Cross*Ours: {oursResult}");
					sw.WriteLine($"Cross*Ours length: {oursResult.Length()}");
					byte[] blah = BitConverter.GetBytes(ours);
					float blah2 = BitConverter.ToSingle(blah);
					sw.WriteLine($"Cross*Ours converted through BitConverter: {blah2} (0x{BitConverter.ToUInt32(BitConverter.GetBytes(blah2)):X8})");
					sw.WriteLine("------");
				}
				sw.Dispose();
				return;
			}

			//Weird niche debug modes that only exist for development
			//FileStream rp = File.Open("E:\\Other Games\\PCSX2173056\\PsychoPS2\\Psychonauts (USA)\\RESOURCE.PAK", FileMode.Open);
			//PS2_FileTable ft = Binary.ReadFromStream<PS2_FileTable>(rp, settings: settings);
			//ft.ExportFiles("D:\\NAUTS_PS2_UNPACKED", PS2_FileNames.FileTable, rp);
			/*
			foreach ( string f in Directory.EnumerateFiles("D:\\NAUTS_PS2_UNPACKED", "*", SearchOption.TopDirectoryOnly))
			{
				byte[] b = File.ReadAllBytes(f);
				if (b[0] == 0x43 && b[1] == 0x59)
				{
					Console.WriteLine($"{f} is a PLB");
					File.Move(f, Path.ChangeExtension(f, "pl2"));
				}
				else Console.WriteLine($"{f} is something else.");
			}
			*/

			/*
			uint hash = PS2_FileEntry.GetFilePathHash(args[0]);
			Console.WriteLine($"{hash:X8}");
			return;
			*/
#endif

			IdentifiedFile? maybeFileData = FileUtils.IdentifyFile(file);
			if ( maybeFileData == null )
			{
				Console.WriteLine("The provided file does not exist, is not a supported file type, or is invalid!");
				Console.ReadKey();
				return;
			}
			
			IdentifiedFile fileData = maybeFileData.Value;

			if (fileData.ppf?.Scene != null)
			{
				ParsePLBData(fileData.ppf.Scene);
			}
			else if ( fileData.plb != null )
			{
				IdentifiedFile? maybeFile2Data = null;
				if (janFile != string.Empty)
				{
					maybeFile2Data = FileUtils.IdentifyFile(janFile);
				}
				AnimToProcess[]? anim = maybeFile2Data?.jan;
				ParsePLBData(fileData.plb, anim);
			}
			else if (fileData.glTF != null)
				GLTFConverter.ConvertAndSaveModelRoot(fileData.glTF);

			Console.WriteLine($"Done!");
		}
	}
}