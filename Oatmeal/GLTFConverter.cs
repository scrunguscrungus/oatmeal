﻿using System.Numerics;
using System.Text.Json.Nodes;

using Newtonsoft.Json.Linq;

using Oatmeal.Constants;

using PsychoPortal;

using SharpGLTF.Geometry;
using SharpGLTF.Geometry.VertexTypes;
using SharpGLTF.Materials;
using SharpGLTF.Scenes;
using SharpGLTF.Schema2;

namespace Oatmeal
{
	internal class GLTFConverter
	{
		internal static void ConvertAndSaveModelRoot(ModelRoot mr)
		{
			glScene glscene = mr.DefaultScene;
			SceneBuilder sb = glscene.ToSceneBuilder();
			//SCENE setup
			Console.WriteLine("Constructing scene...");
			Scene scene = new Scene();
			scene.Version = 319;

			List<TextureReference> translationTable = new List<TextureReference>();
			if (Switches.bEnableLightmapping)
			{
				TextureReference lightmap = new TextureReference()
				{
					FileName = "textures\\Lightmaps\\lightmaptest\\lightmap000.dds",
					RepeatUV = true
				};
				translationTable.Add(lightmap);
			}

			//DOMAIN setup
			Console.WriteLine("Constructing root domain...");
			Domain root = scene.RootDomain = new Domain();
			root.Name = "Dummy";
			root.Pre_Version = 319;
			root.Meshes = new Mesh[1] { new Mesh() };
			root.DomainEntityInfos = new DomainEntityInfo[]
			{
			};

			//MESH setup
			Console.WriteLine("Constructing mesh...");
			Mesh mesh = root.Meshes[0];
			mesh.Pre_Version = 319;
			mesh.Position = new(0, 0, 0);
			mesh.Rotation = new(0, 0, 0);
			mesh.Scale = new(1, 1, 1);
			mesh.LODCount = 1;
			mesh.AnimAffectors = new AnimAffector[0];
			mesh.Name = "DomainFirstMesh";

			string colName = $"{Program.inputFilename}_collision.gltf";
			if (File.Exists(colName))
			{
				Console.WriteLine($"Using collision file: {colName}");
				mesh.HasCollisionTree = 1;
				mesh.CollisionTree = new CollisionTree();
				mesh.CollisionTree.Pre_Version = 319;
				mesh.CollisionTree.Octree = new Octree();
				mesh.CollisionTree.Octree.SSECube = new SSECube()
				{
					Position = new Vec3(0, 0, 0),
					Length = FixedValues.FixedBounds
				};

				ModelRoot cmr = ModelRoot.Load(colName);
				MeshBuilder<MaterialBuilder, VertexPosition, VertexEmpty, VertexEmpty> mb = cmr.DefaultScene.ToStaticMeshBuilder<VertexPosition, VertexEmpty>(null, null, 0f);


				if (mb.Primitives.Count > 1)
				{
					Console.ForegroundColor = ConsoleColor.Cyan;
					Console.WriteLine($"{mb.Primitives.Count} primitives in collision data for mesh 1. They will be merged.");
					Console.ForegroundColor = ConsoleColor.White;
				}

				Console.WriteLine($"Constructing collision vertices...");
				List<Vec3> colVerts = new();
				foreach (var primi in mb.Primitives)
				{
					foreach (var vert in primi.Vertices)
					{
						Vector3 scaled = vert.Position * FixedValues.BlenderToLiPo;
						colVerts.Add(scaled.ToVec3());
					}
				}
				mesh.CollisionTree.Vertices = colVerts.ToArray();

				//PrimitiveBuilder<MaterialBuilder, VertexPosition, VertexEmpty, VertexEmpty> prim = mb.Primitives.First();
				Console.WriteLine($"Constructing collision polys...");
				List<CollisionPoly2> polys = new();
				int lastprimIdx = 0;
				int curPrim = 0;
				uint navIdx = 0;

				List<int> usedIndices = new();
				int UnshareVertexIndex(int index)
				{
					if (usedIndices.Contains(index))
					{
						colVerts.Add(new Vec3(colVerts[index].X, colVerts[index].Y, colVerts[index].Z));
						index = colVerts.Count() - 1;
						mesh.CollisionTree.Vertices = colVerts.ToArray();
					}
					else
					{
						usedIndices.Add(index);
					}
					return index;
				}
				foreach (var prim in mb.Primitives)
				{
					Console.WriteLine($"Integrating primitive {curPrim}: {prim.Material.Name}");
					foreach (var tri in prim.Triangles)
					{
						int index1 = tri.C + lastprimIdx;
						int index2 = tri.B + lastprimIdx;
						int index3 = tri.A + lastprimIdx;

						//Explanation:
						//Psychonauts' collision stuff doesn't seem to really like it when collision triangles share vertices.
						//For example, if you have triangle A and triangle B sharing verts, where triangle B is a relative gravity surface
						//as Raz travels along triangle A towards the shared side, he'll start tilting to triangle B's normal.
						//So, here we have to figure out when triangles in the collision mesh are sharing vertices and duplicate them
						//to new verts, updating the indices and such as we do.
						//
						//This doesn't seem like a perfect solution. It breaks smooth transitions on relgrav surfaces, for example.
						//I've yet to figure out exactly what the right logic is here but this is better than the current behaviour.
						
						index1 = UnshareVertexIndex(index1);
						index2 = UnshareVertexIndex(index2);
						index3 = UnshareVertexIndex(index3);

						Vector3 Vert1 = colVerts[index1].ToVector3();
						Vector3 Vert2 = colVerts[index2].ToVector3();
						Vector3 Vert3 = colVerts[index3].ToVector3();

						Vector3 BA = Vert2 - Vert1;
						Vector3 CA = Vert3 - Vert1;
						Vector3 Cross = Vector3.Cross(CA, BA);

						SurfaceFlags collisionFlags = (SurfaceFlags)ExtrasUtils.GetValue<int>(prim.Material.Extras, "collisionFlags", 0);
						SoundIDs soundID = (SoundIDs)ExtrasUtils.GetValue<int>(prim.Material.Extras, "soundID", (int)SoundIDs.Default);
						byte TriggerID = (byte)ExtrasUtils.GetValue<int>(prim.Material.Extras, "triggerID", 0);
						//Console.WriteLine($"{prim.Material.Name} tid: {TriggerID}");
						CollisionPoly2 newPoly = new CollisionPoly2()
						{
							SurfaceFlags = (SurfaceFlags)collisionFlags,
							SoundID = soundID,
							TriggerSurfaceID = TriggerID,
							VertexIndices = new ushort[]
							{
							(ushort)(index1), (ushort)(index2), (ushort)(index3)
							},
							NormalizationFactor = 1f / Cross.Length(),
							NavMeshIndex = null,
							NavPolyIndex = (UInt24)0
						};

						if (newPoly.SurfaceFlags.HasFlag(SurfaceFlags.Nav))
						{
							newPoly.NavMeshIndex = 0;
							newPoly.NavPolyIndex = new UInt24(navIdx++);
						}

						polys.Add(newPoly);

					}
					curPrim++;
					lastprimIdx += prim.Vertices.Count;
				};

				//Calculated after primitives are added because vertex index unsharing probably added some verts to our total
				if (mesh.CollisionTree.Vertices.Length > ushort.MaxValue)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine($"Collision mesh has too many vertices! Maximum {ushort.MaxValue}, got {mesh.CollisionTree.Vertices.Length}");
					Console.ForegroundColor = ConsoleColor.White;
					Console.ReadKey();
				}
				mesh.CollisionTree.CollisionPolys = polys.ToArray();

				Console.WriteLine($"Finished building collision mesh with {mesh.CollisionTree.Vertices.Length} vertices and {mesh.CollisionTree.CollisionPolys.Length} polys");
				if (mesh.CollisionTree.CollisionPolys.Length > 0x1fff)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine($"!! More than {0x1fff} polys in collision mesh !!\nThis is too much for the dummy octree and will probably crash the game when loaded!");
					Console.ForegroundColor = ConsoleColor.White;
					Console.ReadKey();
				}
				
				mesh.CollisionTree.Bounds = FixedValues.FixedBox;

				mesh.CollisionTree.Octree.LeavesCount = 8;
				mesh.CollisionTree.Octree.NodesCount = 1;

				Console.WriteLine($"Constructing dummy octree...");
				mesh.CollisionTree.Octree.Nodes = new OctreeNode[]
				{
					new OctreeNode()
					{
						Data = new UInt24[]
						{
							new UInt24(0 | OctreeNode.IsLeafFlag),
							new UInt24(1 | OctreeNode.IsLeafFlag),
							new UInt24(2 | OctreeNode.IsLeafFlag),
							new UInt24(3 | OctreeNode.IsLeafFlag),
							new UInt24(4 | OctreeNode.IsLeafFlag),
							new UInt24(5 | OctreeNode.IsLeafFlag),
							new UInt24(6 | OctreeNode.IsLeafFlag),
							new UInt24(7 | OctreeNode.IsLeafFlag),
						}
					}
				};
				List<uint> primIndices = new();
				for (uint i = 0; i < mesh.CollisionTree.CollisionPolys.Length; i++)
				{
					primIndices.Add(i);
				}
				mesh.CollisionTree.Octree.Primitives = primIndices.ToArray();

				mesh.CollisionTree.Octree.Leaves = new OctreeLeaf[]
				{
					new OctreeLeaf(0, (uint)mesh.CollisionTree.Octree.Primitives.Length ),
					new OctreeLeaf(0, (uint)mesh.CollisionTree.Octree.Primitives.Length ),
					new OctreeLeaf(0, (uint)mesh.CollisionTree.Octree.Primitives.Length ),
					new OctreeLeaf(0, (uint)mesh.CollisionTree.Octree.Primitives.Length ),
					new OctreeLeaf(0, (uint)mesh.CollisionTree.Octree.Primitives.Length ),
					new OctreeLeaf(0, (uint)mesh.CollisionTree.Octree.Primitives.Length ),
					new OctreeLeaf(0, (uint)mesh.CollisionTree.Octree.Primitives.Length ),
					new OctreeLeaf(0, (uint)mesh.CollisionTree.Octree.Primitives.Length ),
				};
			}

			List<MeshFrag> frags = new();
			Console.WriteLine($"Adding meshfrags to scene: {sb.Name}");

			foreach (InstanceBuilder ib in sb.Instances)
			{
				ContentTransformer ct = ib.Content;

				if (ct is RigidTransformer rt)
				{
					if ( ExtrasUtils.IsEntity(ct.Extras) )
					{
						DomainEntityInfo newEntity = new DomainEntityInfo();
						newEntity.NewInt1 = -1;
						newEntity.NewInt2 = -1;
						newEntity.UnknownUint = 0;

						newEntity.Name = rt.Name;
						newEntity.Position = new(0, 0, 0);

						if (rt.Transform.Translation != null)
							newEntity.Position = (rt.Transform.Translation.Value * FixedValues.BlenderToLiPo).ToVec3();

						if (rt.Transform.Rotation != null)
							newEntity.Rotation = rt.Transform.Rotation.Value.SafeToVec3() * MathHelpers.Deg2Rad;
						else
							newEntity.Rotation = new Vec3(0, 0, 0);

						if (rt.Transform.Scale != null)
							newEntity.Scale = rt.Transform.Scale.Value.ToVec3();
						else
							newEntity.Scale = new Vec3(1, 1, 1);

						newEntity.ScriptClass = ExtrasUtils.GetValue(ct.Extras, "entprops.classname", "Locator");
						newEntity.EditVars = ExtrasUtils.GetValue(ct.Extras, "entprops.editvars", string.Empty);
						if (newEntity.EditVars != string.Empty)
							newEntity.HasEditVars = 1;

						List<DomainEntityInfo> dei = root.DomainEntityInfos.ToList();
						dei.Add(newEntity);
						root.DomainEntityInfos = dei.ToArray();
						Console.WriteLine($"Added entity: {newEntity.Name} : {newEntity.ScriptClass}");
						Console.WriteLine($"At: {newEntity.Position} : {newEntity.Rotation}");
					}
				}


				if (ct.GetLightAsset() != null && ct is RigidTransformer ft)
				{
					LightBuilder lb = ct.GetLightAsset();
					Console.WriteLine($"Found light: {lb.Name}");
					Light lipolight = new Light();
					lipolight.Name = lb.Name;
					lipolight.Pre_Version = 319;
					lipolight.Color = lb.Color.ToVec3();
					lipolight.Position = (ft.Transform.Translation.Value * FixedValues.BlenderToLiPo).ToVec3();
					lipolight.Settings = new Vec3(1, 0, 0);
					if ( lb is LightBuilder.Point point )
					{
						lipolight.Type = Light.LightType.Point;
						lipolight.Settings = new Vec3(1f, 0.00125f, 0);
					}
					else if (lb is LightBuilder.Spot spot)
					{
						Console.WriteLine("Adding spotlight");
						lipolight.Type = Light.LightType.Spot;
						lipolight.Settings = new Vec3(1, 0, 0);
						lipolight.SpotlightFloat1 = 10f;
						lipolight.SpotlightFloat2 = 5f;
						lipolight.Direction = new Vec3(0, 0, 0);
						if ( ft.Transform.Rotation != null )
							lipolight.Direction = ft.Transform.Rotation.Value.SafeToVec3() * MathHelpers.Deg2Rad;
						Console.WriteLine(lipolight.Direction);
					}
					if (lb is LightBuilder.Directional sun)
					{
						lipolight.Type = Light.LightType.Directional;
						lipolight.Settings = new Vec3(1, 0, 0);
						lipolight.Direction = new Vec3(0,0,0);
						if (ft.Transform.Rotation != null)
							lipolight.Direction = ft.Transform.Rotation.Value.SafeToVec3() * MathHelpers.Deg2Rad;
					}
					if ( Switches.bEnableLightmapping )
					{
						lipolight.Flags |= Light.LightFlags.Dynamic;
					}
					
					//Makes everything yellow??
					//lipolight.Flags |= Light.LightFlags.Flag_7;

					List<Light> curLights;
					if (mesh.Lights != null)
						curLights = mesh.Lights.ToList();
					else
						curLights = new();

					curLights.Add(lipolight);
					mesh.Lights = curLights.ToArray();
					continue;
				}


				if (ct.GetGeometryAsset() == null)
				{
					continue;
				}

				Console.WriteLine($"Constructing mesh {ib.Name}...");

				NodeBuilder nb = ct.GetArmatureRoot();
				bool bActuallyRigged = false;
				if (nb != null)
				{
					Console.WriteLine("Checking for skeleton root...");
					NodeBuilder? rootJoint = null;

					if (ExtrasUtils.GetValue(nb.Extras, "isSkeletonRoot", false))
					{
						rootJoint = nb;
						bActuallyRigged = true;
					}
					else
					{
						try
						{
							rootJoint = nb.VisualChildren.First(x =>
							{
								Node n = mr.LogicalNodes.First(y => y.Name == x.Name);
								

								return ExtrasUtils.GetValue(n.Extras, "isSkeletonRoot", false);
							});
							bActuallyRigged = true;
						}
						catch (InvalidOperationException)
						{
							Console.WriteLine("None found - static mesh.");
							bActuallyRigged = false;
						}
					}

					if (bActuallyRigged && rootJoint != null)
					{
						bool bAnimated = false;
						Console.WriteLine($"Constructing skeleton...");
						Skeleton skeleton = new Skeleton();
						skeleton.Name = nb.Name;

						uint iTotalJoints = 0;

						Joint RecursivelyAddJoints(NodeBuilder gltfJoint, Joint? parent = null)
						{
							//If any nodebuilder has animations...
							if (gltfJoint.HasAnimations)
								bAnimated = true;

							Joint j = new Joint();
							j.Name = gltfJoint.Name;
							j.ID = new JointID();
							j.ID.JointIndex = (int)iTotalJoints;
							iTotalJoints++;
							j.ID.SkeletonIndex = 0;

							if (parent == null)
								Console.WriteLine($"Added joint {j.Name} ({j.ID.JointIndex}) as root");
							else
								Console.WriteLine($"Added joint {j.Name} ({j.ID.JointIndex}) as child of {parent.Name} ({parent.ID.JointIndex})");

							j.Position = gltfJoint.LocalTransform.Translation.ToVec3() * FixedValues.BlenderToLiPo;
							j.Rotation = gltfJoint.LocalTransform.Rotation.SafeToVec3();

							List<Joint> children = new();
							foreach (NodeBuilder nb in gltfJoint.VisualChildren)
							{
								children.Add(RecursivelyAddJoints(nb, j));
							}
							j.Children = children.ToArray();

							return j;
						}

						skeleton.RootJoint = RecursivelyAddJoints(rootJoint);
						skeleton.JointsCount = iTotalJoints;
						mesh.Skeletons = new Skeleton[] { skeleton };
						/*
						if ( bAnimated )
						{
							Console.WriteLine("Animations:");
							int animCount = rootJoint.AnimationTracksNames.Count();
							for ( int i = 0; i < animCount; i++ )
							{
								
							}

							SharedSkelAnim anim = new();
							anim.Header = new();
							anim.Header.JointsCount = (byte)skeleton.JointsCount;
							anim.Header.Version = 201;
							anim.Header.V201_Flags = 8;

							anim.Header.Value2 = anim.Header.Value3 = anim.Header.Value4 = anim.Header.JointsCount;

							anim.Header.V201_TranslateMode = 0;

							anim.RootJoint = new SharedJointAnim();
							if ( rootJoint.Translation.IsAnimated )
							{
								rootJoint.Translation.get
							}
						}
						*/
					}
				}

				IMeshBuilder<MaterialBuilder> mb = ct.GetGeometryAsset();
				IReadOnlyCollection<IPrimitiveReader<MaterialBuilder>> prims = mb.Primitives;

				Console.WriteLine($"Constructing MeshFrags...");
				foreach (IPrimitiveReader<MaterialBuilder> prim in prims)
				{
					Console.WriteLine("Constructing MeshFrag...");
					string texName;
					texName = ExtrasUtils.GetValue(prim.Material.Extras, "texturePath", prim.Material.Name ?? "NO_TEXTURE");

					texName = texName.ToLower();

					if (!translationTable.Any(x => x.FileName.Value == texName))
					{
						TextureReference texRef = new TextureReference();
						Console.WriteLine($"Adding texref: {texName}");

						texRef.FileName = texName;
						texRef.RepeatUV = true;
						translationTable.Add(texRef);
					}


					int texIndex = translationTable.IndexOf(translationTable.Where(x => x.FileName.Value == texName).First());

					List<VertexNotexNorm> verts = new();
					List<short> idxBuffer = new();
					List<UVSet> uvSets = new();

					MeshFrag mf = new MeshFrag();
					mf.MaterialColor = new(1, 1, 1, 1);
					mf.TextureIndices = new int[] { texIndex };
					mf.DetailTextureIndex = -1;
					mf.GlossmapTextureIndex = -1;
					mf.UVScale = 1.0f;
					mf.AnimInfo = null;
					mf.MaterialFlags = ExtrasUtils.GetValue(prim.Material.Extras, "materialFlags", MaterialFlags.None);
					mf.MaterialFlags |= ExtrasUtils.GetValue(prim.Material.Extras, "flag31", false) ? MaterialFlags.Flag_31 : 0;

					mf.TexCoordTransVel = ExtrasUtils.GetValue(prim.Material.Extras, "texTransVel", new Vec2(0.0f, 0.0f));

					if (Switches.bEnableLightmapping)
					{
						mf.MaterialFlags |= MaterialFlags.Lightmap;
						mf.LightMapTextureIndices = new int[] { 0, -1 };
					}


					SkinWeight[] weights = new SkinWeight[0];
					List<JointID> usedJoints = new();
					int ivert = 0;
					float maxUV = 0.0f;
					foreach (IVertexBuilder? glvtx in prim.Vertices.Where(x => x.GetMaterial().MaxTextCoords > 0))
					{
						Vector2 coord = glvtx.GetMaterial().GetTexCoord(0);

						float max = Math.Max(Math.Abs(coord.X), Math.Abs(coord.Y));
						maxUV = Math.Max(maxUV, max);
					}

					mf.UVScale = maxUV;
					Console.WriteLine($"Calculated UV scale: {mf.UVScale}");

					foreach (IVertexBuilder? glvtx in prim.Vertices)
					{
						VertexNotexNorm vtx = new VertexNotexNorm();
						vtx.Vertex = glvtx.GetGeometry().GetPosition().ToVec3() * FixedValues.BlenderToLiPo;
						vtx.Normal = new NormPacked3();
						vtx.Normal.Value = 0;

						Vector3 normal;
						if (glvtx.GetGeometry().TryGetNormal(out normal))
						{
							vtx.Normal = NormPacked3.FromVec3(normal.ToVec3());
						}
						verts.Add(vtx);
						if (glvtx.GetMaterial().MaxTextCoords > 0)
						{
							mf.UVChannelsCount = (uint)glvtx.GetMaterial().MaxTextCoords;
							UVSet wtf = new UVSet();
							wtf.UVs = new UV[glvtx.GetMaterial().MaxTextCoords];

							for (int i = 0; i < glvtx.GetMaterial().MaxTextCoords; i++)
							{
								Vector2 coord = glvtx.GetMaterial().GetTexCoord(i);
								UV texCoord = new UV();
								texCoord.U = (short)((coord.X / mf.UVScale) * 0x7fff);
								texCoord.V = (short)((coord.Y / mf.UVScale) * 0x7fff);

								wtf.UVs[i] = texCoord;
							}

							uvSets.Add(wtf);
						}
						else
						{
							int ourSet = uvSets.AddGetIndex(new());
							UV texCoord = new UV();
							texCoord.U = 0;
							texCoord.V = 0;

							uvSets[ourSet].UVs = new UV[] { texCoord };
						}

						IVertexSkinning skinning = glvtx.GetSkinning();

						if (skinning != null && skinning is not VertexEmpty && mesh.Skeletons != null && mesh.Skeletons.Length > 0)
						{
							if (mf.AnimInfo == null)
							{
								Console.WriteLine("Skinning data detected, constructing ModelAnimInfo...");
								mf.AnimInfo = new ModelAnimInfo();
								mf.AnimInfo.InfluencesPerVertex = 2;

								weights = new SkinWeight[prim.Vertices.Count];
								usedJoints = new();

								mf.MaterialFlags |= MaterialFlags.Skinned;
							}
							if (skinning.JointsLow.Z != 0 || skinning.JointsLow.W != 0 || skinning.WeightsLow.Z != 0 || skinning.WeightsLow.W != 0)
							{
								Console.ForegroundColor = ConsoleColor.Yellow;
								Console.WriteLine("!! More than 2 joints assigned to vertex. Psychonauts doesn't support this!\nData will be discarded...");
								Console.ForegroundColor = ConsoleColor.White;
							}


							int joint1 = (int)skinning.JointsLow.X;
							int joint2 = (int)skinning.JointsLow.Y;

							//Unsure if this is a Blender exporter or a general glTF thing,
							//but when a vertex has no bone assignments it gets assigned to a phantom
							//joint with the index 1 above the number of actual joints.
							//We just link these into joint 0 and give them 0 weight.
							uint max = mesh.Skeletons[0].JointsCount - 1;
							bool j1Valid = joint1 <= max;
							bool j2Valid = joint2 <= max;

							if (!j1Valid)
								joint1 = 0;
							if (!j2Valid)
								joint2 = 0;

							//Console.WriteLine($"break here ya bastard");

							//If these joints aren't in the list, add them.

							int joint1Idx = usedJoints.FindIndex(x => x.JointIndex == joint1);
							if (joint1Idx == -1)
								joint1Idx = usedJoints.AddGetIndex(new JointID() { JointIndex = joint1, SkeletonIndex = 0 });

							int joint2Idx = usedJoints.FindIndex(x => x.JointIndex == joint2);
							if (joint2Idx == -1)
								joint2Idx = usedJoints.AddGetIndex(new JointID() { JointIndex = joint2, SkeletonIndex = 0 });

							float weight1 = j1Valid ? skinning.WeightsLow.X : 0;
							float weight2 = j2Valid ? skinning.WeightsLow.Y : 0;

							SkinWeight wght = new SkinWeight();
							wght.Joint1 = joint1Idx;
							wght.Joint2 = joint2Idx;
							wght.Weight = new Vec2(weight1, weight2);
							weights[ivert] = wght;
						}

						ivert++;
					}

					if (verts.Count > short.MaxValue)
					{
						Console.ForegroundColor = ConsoleColor.Red;
						Console.WriteLine($"Meshfrag has more than {short.MaxValue} vertices.\nverts.Count: {verts.Count}\nprim.Vertices.Count: {prim.Vertices.Count}");
						Console.WriteLine($"Primitives in this meshfrag will not be able to correctly index their vertices.");
						Console.ReadKey();
						Console.ForegroundColor = ConsoleColor.White;
					}
					else
					{
						Console.ForegroundColor = ConsoleColor.Cyan;
						Console.WriteLine($"Finished building meshfrag with {verts.Count} vertices.");
						Console.ForegroundColor = ConsoleColor.White;
					}

					if (mf.AnimInfo != null)
					{
						Console.WriteLine("Finalising ModelAnimInfo...");
						mf.AnimInfo.BonesCount = (ushort)usedJoints.Count;
						mf.AnimInfo.JointIDs = usedJoints.ToArray();
						mf.AnimInfo.OriginalSkinWeights = weights;
						mf.AnimInfo.BoneMaxWeights = Enumerable.Repeat(new BoneMaxWeight(), usedJoints.Count).ToArray();

						//Don't actually know what these are for or how they're set up.
						//2024.05.06 Documents/ProgrammingDocs/Psychonauts Optimization List.xls
						//This might be related to "Animated Mesh Fragment Bounding Boxes"? No idea.
						mf.AnimInfo.BoneBounds = new Box3[usedJoints.Count];

						int idx = 0;
						foreach (int jointIdx in usedJoints.Select(x => x.JointIndex))
						{
							mf.AnimInfo.BoneBounds[idx] = FixedValues.FixedBox;

							idx++;
						}
					}

					foreach ((int A, int B, int C) index in prim.Triangles)
					{
						mf.PolygonCount++;
						idxBuffer.Add((short)index.C);
						idxBuffer.Add((short)index.B);
						idxBuffer.Add((short)index.A);
					}

					mf.Vertices = verts.ToArray();

					mf.Bounds = FixedValues.FixedBox;

					mf.PolygonIndexBuffer = idxBuffer.ToArray();

					mf.Vector_68 = new(1, 1, 1);

					Console.WriteLine($"Meshfrag has {mf.UVChannelsCount} UV channels");

					mf.UVSets = uvSets.ToArray();

					frags.Add(mf);
				} //Primitive
			} //Instance
			mesh.MeshFrags = frags.ToArray();

			mesh.Bounds = FixedValues.FixedBox;

			scene.RootDomain.Bounds = FixedValues.FixedBox;

			translationTable.ForEach(x => {
				if (!x.FileName.Value.Contains("workresource/textures") && !x.FileName.Value.StartsWith("textures\\") && !x.FileName.Value.StartsWith("textures/"))
				{
					Console.WriteLine($"Fixing up texture reference {x.FileName}:");
					x.FileName = $"textures\\{x.FileName.Value.ToLower()}.dds";
					Console.WriteLine($"{x.FileName.Value.ToLower()}");
				}

			});
			scene.TextureTranslationTable = translationTable.ToArray();

			string plbOutName = String.Format($"{Program.inputFilename}.plb");

			foreach (string f in Directory.EnumerateFiles("./", plbOutName + ".plb"))
			{
				File.Delete(f);
			}
			Console.WriteLine($"Saving {plbOutName}...");
			using (BinarySerializerLogger logger = new BinarySerializerLogger("./SERIALIZATION.log"))
			{
				using (FileStream fs = File.OpenWrite(plbOutName))
				{
					PsychonautsSettings settings = new(PsychonautsVersion.PC_Digital);
					Binary.WriteToStream<Scene>(scene, fs, settings: settings, logger: logger);
				}
			}
		}
	}
}
