﻿using System.Numerics;
using System.Text.Json.Nodes;

using Newtonsoft.Json;

using Oatmeal.Constants;

using PsychoPortal;

using SharpGLTF.Animations;
using SharpGLTF.Geometry;
using SharpGLTF.Geometry.VertexTypes;
using SharpGLTF.Materials;
using SharpGLTF.Scenes;
using SharpGLTF.Schema2;
using SharpGLTF.Transforms;

namespace Oatmeal
{
	static class PLBConverter
	{
		static AffineTransform TransformFromPsychonauts(Vec3 pos, Vec3? rot = null, Vec3? scale = null)
		{
			Vector3 posC = pos.ToVector3();
			Quaternion rotC;
			Vector3 scaleC;

			if (rot == null)
				rotC = Quaternion.Identity;
			else
				rotC = rot.ToQuaternion();

			if (scale == null)
				scaleC = Vector3.One;
			else
				scaleC = scale.ToVector3();

			AffineTransform at = new AffineTransform(
				scaleC,
				rotC,
				posC
				);

			return at;
		}

		static NodeBuilder RecursivelyAddJoint(Joint joint, ref Dictionary<int, NodeBuilder> globalList, NodeBuilder? parent = null, Joint? jointParent = null)
		{
			NodeBuilder thisJoint;
			if (parent != null && jointParent != null)
			{
				thisJoint = parent.CreateNode(joint.Name);
				Console.WriteLine($"Attaching {joint.Name} ({joint.ID.JointIndex}) to {parent.Name} ({jointParent.ID.JointIndex})");
			}
			else
			{
				thisJoint = new NodeBuilder(joint.Name);
				Console.WriteLine($"{joint.Name} ({joint.ID.JointIndex}) is the root!");
			}

			Vec3 pos = joint.Position;
			Vec3 rot = joint.Rotation;

			Quaternion rotQuat = rot.ToQuaternion(); //AUTOMATICALLY BECOMES RADIANS!

			thisJoint.WithLocalRotation(rotQuat)
				.WithLocalTranslation(pos.ToVector3() * FixedValues.LiPoToBlender);

			globalList.Add(joint.ID.JointIndex, thisJoint);
			foreach (Joint j in joint.Children)
			{
				RecursivelyAddJoint(j, ref globalList, thisJoint, joint);
			}
			return thisJoint;
		}

		static void VerifyJointIDs(Joint start, List<NodeBuilder> gltfSkel)
		{
			string us = start.Name;
			int usID = start.ID.JointIndex;

			string them = gltfSkel[usID].Name;

			if (us != them)
			{
				throw new Exception($"JOINT VERIFICATION FAILED! Joint {usID} should have been {us}, but was {them}!");
			}
			else
			{
				Console.WriteLine($"Successfully verified joint {usID}: LiPo Joint {usID} {us} == GLTF Node {usID} {them}");
			}

			foreach (Joint j in start.Children)
			{
				VerifyJointIDs(j, gltfSkel);
			}
		}


		public static Tuple<NodeBuilder?, Dictionary<int, NodeBuilder>, List<NodeBuilder>> BuildSkeleton(Skeleton skellington)
		{
			NodeBuilder? rootJoint = null;
			Dictionary<int, NodeBuilder> fullSkeletonDict = new Dictionary<int, NodeBuilder>();
			List<NodeBuilder> fullSkeleton = new List<NodeBuilder>();

			rootJoint = RecursivelyAddJoint(skellington.RootJoint, ref fullSkeletonDict);
			rootJoint.Extras = ExtrasUtils.SetValue(rootJoint.Extras, "isSkeletonRoot", true);

			//Ordering might already be correct but doesn't hurt to be safe
			foreach (KeyValuePair<int, NodeBuilder> kv in fullSkeletonDict.OrderBy(x => x.Key))
			{
				fullSkeleton.Add(kv.Value);
			}

			VerifyJointIDs(skellington.RootJoint, fullSkeleton);
			Console.WriteLine("Joint indices verified! Skeleton is OK!!!!");

			return new(rootJoint, fullSkeletonDict, fullSkeleton);
		}

		//Recursive
		public static void ConvertAndSaveDomainMeshes(Scene scn, Domain d, AnimToProcess[]? anim = null)
		{
			Console.WriteLine($"Parsing meshes from domain: {d.Name}");
			Mesh[] meshes = d.Meshes;

			Console.WriteLine($"{meshes.Length} meshes in domain");
			if (anim != null && meshes.Length > 1)
				Console.WriteLine("Anim mode but multiple meshes, all but first will be skipped!");

			List<SceneBuilder> meshScenes = new List<SceneBuilder>(); //List of scenes containing all the different meshes parsed from this domain
			for (int i = 0; i < meshes.Length; i++)
			{
				if (anim != null && i > 0)
					continue;

				Mesh mesh = meshes[i];

				Console.WriteLine($"Creating root scene");
				SceneBuilder meshRoot = new SceneBuilder(mesh.Name);

				Console.WriteLine($"{mesh.MeshFrags.Length} meshfrags");

				if (mesh.HasCollisionTree == 1)
				{
					SceneBuilder collisionScene = ConvertCollision(mesh);
					ModelRoot model = collisionScene.ToGltf2();

					string collisionOutName = String.Format(Strings.outFileFmt, Program.inputFilename, d.Name, mesh.Name + "_collision");
					foreach (string f in Directory.EnumerateFiles("./", collisionOutName + ".gltf"))
					{
						File.Delete(f);
					}
					foreach (string f in Directory.EnumerateFiles("./", collisionOutName + ".bin"))
					{
						File.Delete(f);
					}

					Console.WriteLine($"Saving {collisionOutName}...");
					model.SaveGLTF(collisionOutName + ".gltf");
				}

				SceneBuilder allMeshFrags = new SceneBuilder();
				NodeBuilder? rootJoint = null;
				Dictionary<int, NodeBuilder> fullSkeletonDict = new Dictionary<int, NodeBuilder>();
				List<NodeBuilder> fullSkeleton = new List<NodeBuilder>();
				if (mesh.Skeletons.Length > 0 && Switches.bEnableSkinning)
				{
					Console.WriteLine($"Model's got {mesh.Skeletons.Length} skeletons, setting up joints");
					Tuple<NodeBuilder?, Dictionary<int, NodeBuilder>, List<NodeBuilder>> result = BuildSkeleton(mesh.Skeletons[0]);
					rootJoint = result.Item1;
					fullSkeletonDict = result.Item2;
					fullSkeleton = result.Item3;
				}

				if (anim != null)
				{
					foreach (AnimToProcess a in anim )
					{
						AnimUtils.ProcessSkelAnim(a, fullSkeleton[0], mesh);
					}
				}

				Console.WriteLine($"Parsing meshfrags");
				foreach (MeshFrag frag in mesh.MeshFrags)
				{
					if (frag.DistantLOD > 1)
						continue;

					MeshBuilder<VERTEX, VertexColor1Texture2, VertexJoints4> mb = new MeshBuilder<VERTEX, VertexColor1Texture2, VertexJoints4>();
					
					string myMatName = string.Empty;
					MaterialBuilder myMat = new MaterialBuilder();

					foreach (int texIndex in frag.TextureIndices)
					{
						myMatName = scn.TextureTranslationTable[texIndex].FileName;
						myMat.Name = $"{Path.GetFileName(myMatName)}_{myMat.GetHashCode()}";
						
						myMat.WithMetallicRoughnessShader()
							.WithMetallicRoughness(0.0f, 1.0f)
							.WithBaseColor(frag.MaterialColor.ToVector4());
					}

					var meshExtras = new
					{
						texturePath = myMatName
					};

					myMat.Extras = ExtrasUtils.SetValue(myMat.Extras, "texturePath", myMatName);

					//Here we remove a bunch of flags that BrainTumbler does not expose as flags in Blender
					//These are mostly flags that are set conditionally, based on what the material actually contains
					//As such GLTFConverter sets them during conversion when relevant.
					MaterialFlags adjustedMatFlags = frag.MaterialFlags;
					adjustedMatFlags &= ~MaterialFlags.Skinned;
					adjustedMatFlags &= ~MaterialFlags.Bumpmap;
					adjustedMatFlags &= ~MaterialFlags.Lightmap;
					adjustedMatFlags &= ~MaterialFlags.Specular;
					adjustedMatFlags &= ~MaterialFlags.ENVMap;
					adjustedMatFlags &= ~MaterialFlags.Tristrip;
					adjustedMatFlags &= ~MaterialFlags.DetailTexture;
					adjustedMatFlags &= ~MaterialFlags.Flag_31;

					myMat.Extras = ExtrasUtils.SetValue(myMat.Extras, "materialFlags", (uint)adjustedMatFlags);
					myMat.Extras = ExtrasUtils.SetValue(myMat.Extras, "flag31", frag.MaterialFlags.HasFlag(MaterialFlags.Flag_31));

					myMat.Extras = ExtrasUtils.SetVec2(myMat.Extras, "texTransVel", frag.TexCoordTransVel);

					PrimitiveBuilder<MaterialBuilder, VERTEX, VertexColor1Texture2, VertexJoints4> prim = mb.UsePrimitive(myMat);

					Console.WriteLine($"Creating vertices");
					//TODO:
					//Rather than conversion here, we should use 
					int[] indices = !frag.MaterialFlags.HasFlag(MaterialFlags.Tristrip) ? frag.PolygonIndexBuffer.Select(x => (int)x).ToArray() : MeshHelpers.TriStripToTriangles(frag.PolygonIndexBuffer, flip: false);
					for (int idx = 0; idx < indices.Length; idx += 3)
					{
						int[] vertIdx = { indices[idx], indices[idx + 1], indices[idx + 2] };

						VertexNotexNorm[] vertices = { frag.Vertices[indices[idx]],
														frag.Vertices[indices[idx+1]],
														frag.Vertices[indices[idx+2]]};

						BGRA8888Color[] colours = new BGRA8888Color[3];

						if (frag.HasVertexColors == 1)
						{
							colours = new BGRA8888Color[3] {
								frag.VertexColors[indices[idx]],
								frag.VertexColors[indices[idx + 1]],
								frag.VertexColors[indices[idx + 2]]

							};
						}


						Vec2[] uvs;
						if (frag.UVSets[indices[idx]].UVs.Length > 1)
						{
							uvs = new Vec2[] {
								frag.UVSets[indices[idx]].UVs[0].ToVec2(frag.UVScale),
								frag.UVSets[indices[idx + 1]].UVs[0].ToVec2(frag.UVScale),
								frag.UVSets[indices[idx + 2]].UVs[0].ToVec2(frag.UVScale),
								frag.UVSets[indices[idx]].UVs[1].ToVec2(frag.UVScale),
								frag.UVSets[indices[idx + 1]].UVs[1].ToVec2(frag.UVScale),
								frag.UVSets[indices[idx + 2]].UVs[1].ToVec2(frag.UVScale)
							};
						}
						else
						{
							uvs = new Vec2[]
							{
								frag.UVSets[indices[idx]].UVs[0].ToVec2(frag.UVScale),
								frag.UVSets[indices[idx+1]].UVs[0].ToVec2(frag.UVScale),
								frag.UVSets[indices[idx+2]].UVs[0].ToVec2(frag.UVScale),
								new Vec2(0, 0),
								new Vec2(0, 0),
								new Vec2(0, 0)
							};
						}

						Vector4[] skinJoints = { Vector4.Zero, Vector4.Zero, Vector4.Zero };
						Vector4[] skinWeights = { new Vector4(1, 0, 0, 0), new Vector4(1, 0, 0, 0), new Vector4(1, 0, 0, 0) };
						if (rootJoint != null)
						{
							if (frag.AnimInfo != null)
							{
								//JointIDs is an array of joints associated with this frag
								//Weights is a per-vertex set of information about the weighting of each vertex
								//Weights[vertex index].Joint1 is the index in jointIDs of the first joint associated with that vertex index, etc.

								JointID[] jointIDs = frag.AnimInfo.JointIDs;
								SkinWeight[] weights = frag.AnimInfo.OriginalSkinWeights;

								int v1 = vertIdx[0];
								int v2 = vertIdx[1];
								int v3 = vertIdx[2];

								skinJoints = new Vector4[3] {
								new Vector4(jointIDs[weights[v1].Joint1].JointIndex, jointIDs[weights[v1].Joint2].JointIndex, 0, 0),
								new Vector4(jointIDs[weights[v2].Joint1].JointIndex, jointIDs[weights[v2].Joint2].JointIndex, 0, 0),
								new Vector4(jointIDs[weights[v3].Joint1].JointIndex, jointIDs[weights[v3].Joint2].JointIndex, 0, 0)
								};

								skinWeights = new Vector4[3]
								{
								new Vector4(weights[v1].Weight.X, weights[v1].Weight.Y, 0, 0),
								new Vector4(weights[v2].Weight.X, weights[v2].Weight.Y, 0, 0),
								new Vector4(weights[v3].Weight.X, weights[v3].Weight.Y, 0, 0),
								};
							}
							else
							{
								Console.WriteLine("!! Mesh has skeleton but no AnimInfo on meshfrag? Malformed?");
							}
						}


						VertexBuilder<VERTEX, VertexColor1Texture2, VertexJoints4>[] vbs = new VertexBuilder<VERTEX, VertexColor1Texture2, VertexJoints4>[3];
						
						for (int k = 0; k < vertices.Length; k++)
						{
							vbs[k] = new VertexBuilder<VERTEX, VertexColor1Texture2, VertexJoints4>();
							vbs[k].Geometry = new VERTEX(vertices[k].Vertex.X * FixedValues.LiPoToBlender, vertices[k].Vertex.Y * FixedValues.LiPoToBlender, vertices[k].Vertex.Z * FixedValues.LiPoToBlender,
							vertices[k].Normal.ToVec3().X, vertices[k].Normal.ToVec3().Y, vertices[k].Normal.ToVec3().Z);

							//Now UVs and vertex colours
							//PC doesn't have vertex colours but I guess we can support PS2 as well?
							//Would be useful for pulling over unused models accidentally shipped on pS2

							Vector4 col;
							if (frag.HasVertexColors == 1)
							{
								col = new Vector4(colours[k].Red / 255, colours[k].Blue / 255, colours[k].Green / 255, colours[k].Alpha / 255);
							}
							else
							{
								col = Vector4.One;
							}
							Vec2 uvVec2 = uvs[k];
							Vec2 uvVec2_second = uvs[k+3];
							vbs[k].Material = new VertexColor1Texture2(col, new Vector2(uvVec2.X, uvVec2.Y), new Vector2(uvVec2_second.X, uvVec2_second.Y));

							//Skinning
							vbs[k].Skinning.Joints = skinJoints[k];
							vbs[k].Skinning.Weights = skinWeights[k];
						}

						prim.AddTriangle(
							vbs[2], vbs[1], vbs[0]
							);
					}


					//Console.ReadKey();
					Console.WriteLine("Adding meshfrag");
					if (rootJoint != null && Switches.bEnableSkinning)
						allMeshFrags.AddSkinnedMesh(mb, Matrix4x4.Identity, fullSkeleton.ToArray());
					else
						allMeshFrags.AddRigidMesh(mb, Matrix4x4.Identity);
				}

				meshRoot.AddScene(allMeshFrags, Matrix4x4.Identity);

				meshScenes.Add(meshRoot);
			} //Meshes

			for (int i = 0; i < meshScenes.Count; i++)
			{
				string gltfOutName = String.Format(Strings.outFileFmt, Program.inputFilename, d.Name, meshScenes[i].Name);
				
				foreach (string f in Directory.EnumerateFiles("./", gltfOutName + ".gltf"))
				{
					File.Delete(f);
				}
				foreach (string f in Directory.EnumerateFiles("./", gltfOutName + ".bin"))
				{
					File.Delete(f);
				}

				ModelRoot model = meshScenes[i].ToGltf2();

				Console.WriteLine($"Saving {gltfOutName}...");
				model.SaveGLTF(gltfOutName + ".gltf");
			}

			foreach (Domain child in d.Children)
			{
				ConvertAndSaveDomainMeshes(scn, child, anim);
			}
		}

		private static SceneBuilder ConvertCollision(Mesh mesh)
		{
			Console.WriteLine("Converting collision...");
			SceneBuilder collisionScene = new SceneBuilder();

			Vec3[] collisionVerts = mesh.CollisionTree.Vertices;
			CollisionPoly2[] collisionPolys = mesh.CollisionTree.CollisionPolys;
			MeshBuilder<VertexPosition, VertexEmpty, VertexEmpty> mb = new MeshBuilder<VertexPosition, VertexEmpty, VertexEmpty>();
			MaterialBuilder mat = new MaterialBuilder();

			PrimitiveBuilder<MaterialBuilder, VertexPosition, VertexEmpty, VertexEmpty> prim = mb.UsePrimitive(mat);
			foreach (CollisionPoly2 poly in collisionPolys)
			{
				Vec3[] verts;
				verts = new Vec3[]
				{
							collisionVerts[poly.VertexIndices[0]],
							collisionVerts[poly.VertexIndices[1]],
							collisionVerts[poly.VertexIndices[2]]
				};

				VertexBuilder<VertexPosition, VertexEmpty, VertexEmpty>[] vbs = new VertexBuilder<VertexPosition, VertexEmpty, VertexEmpty>[3];

				//Build geo vertices
				for (int k = 0; k < verts.Length; k++)
				{
					vbs[k] = new VertexBuilder<VertexPosition, VertexEmpty, VertexEmpty>();
					vbs[k].Geometry = new VertexPosition(verts[k].X * FixedValues.LiPoToBlender, verts[k].Y * FixedValues.LiPoToBlender, verts[k].Z * FixedValues.LiPoToBlender);
				}

				prim.AddTriangle(vbs[2], vbs[1], vbs[0]);
			}

			collisionScene.AddRigidMesh(mb, Matrix4x4.Identity);
			return collisionScene;
		}
	}
}
