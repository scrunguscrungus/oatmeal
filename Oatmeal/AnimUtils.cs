﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Oatmeal.Constants;
using PsychoPortal;
using SharpGLTF.Animations;

using SharpGLTF.Scenes;

namespace Oatmeal
{
	struct AnimToProcess
	{
		public string name;
		public SharedSkelAnim anim;
	}

	internal static class AnimUtils
	{

		public static AnimToProcess ProcessGLTFAnims(NodeBuilder animRoot)
		{
			throw new NotImplementedException("todo");
		}

		private static void AddDataForJoint(string name, SharedJointAnim animJoint, NodeBuilder animnode)
		{
			//NodeBuilder animnode = getJointByName(animJoint.Name);

			IEnumerable<SharedJointAnim_Channel> translationChannels = animJoint.Channels.Where(x => x.Type == SharedJointAnim_ChannelType.Position);
			IEnumerable<SharedJointAnim_Channel> rotationChannels = animJoint.Channels.Where(x => x.Type == SharedJointAnim_ChannelType.Rotation);
			IEnumerable<SharedJointAnim_Channel> scaleChannels = animJoint.Channels.Where(x => x.Type == SharedJointAnim_ChannelType.Scale);

			string animName = name;

			for (int c = 0; c < translationChannels.Count(); c++)
			{
				SharedJointAnim_Channel channel = translationChannels.ElementAt(c);

				CurveBuilder<Vector3> translationCurve = animnode.UseTranslation(animName);

				//Populate translation
				for (int kf = 0; kf < channel.KeyFrames.Length; kf++)
				{
					SharedJointAnim_KeyFrame keyframe = channel.KeyFrames[kf];

					float time = (float)keyframe.Time / 30;

					translationCurve.SetPoint(time, keyframe.Position.ToVector3() * FixedValues.LiPoToBlender);
					Console.WriteLine($"{animName} {animJoint.Name}: Added position keyframe {time} {keyframe.Position.ToVector3() * FixedValues.LiPoToBlender}");
				}
			}
			for (int c = 0; c < rotationChannels.Count(); c++)
			{
				SharedJointAnim_Channel channel = rotationChannels.ElementAt(c);

				CurveBuilder<Quaternion> rotationCurve = animnode.UseRotation(animName);

				//Populate rotation
				for (int kf = 0; kf < channel.KeyFrames.Length; kf++)
				{
					SharedJointAnim_KeyFrame keyframe = channel.KeyFrames[kf];

					float time = (float)keyframe.Time / 30;

					rotationCurve.SetPoint(time, keyframe.Rotation.ToQuaternion());
					Console.WriteLine($"{animName} {animJoint.Name}: Added rotation keyframe {time} {keyframe.Rotation.ToQuaternion()}");
				}
			}

			for (int c = 0; c < scaleChannels.Count(); c++)
			{
				SharedJointAnim_Channel channel = scaleChannels.ElementAt(c);

				CurveBuilder<Vector3> scaleCurve = animnode.UseScale(animName);

				//Populate scale
				for (int kf = 0; kf < channel.KeyFrames.Length; kf++)
				{
					SharedJointAnim_KeyFrame keyframe = channel.KeyFrames[kf];

					float time = (float)keyframe.Time / 30;

					Vector3 ourScale = keyframe.Scale.ToVector3();

					//Hack: Psychonauts is a little goofy with its bone scaling
					if (animnode.Parent != null && animnode.Parent.UseScale().IsAnimated)
					{
						Console.WriteLine($"{animName} Global scale: {ourScale}");
						CurveBuilder<Vector3> parentScaleTrack = animnode.Parent.UseScale(animName);
						Vector3 parentScaleAtThisTime = parentScaleTrack.GetPoint(time);
						Console.WriteLine($"{animName} Parent's scale: {parentScaleAtThisTime}");

						Vector3 diff = ourScale - parentScaleAtThisTime;

						ourScale -= diff;

						Console.WriteLine($"{animName} Calculated local scale for scale keyframe: {ourScale}");
					}

					scaleCurve.SetPoint(time, ourScale);
					Console.WriteLine($"{animName} {animJoint.Name}: Added scale keyframe {time} {keyframe.Scale.ToVector3()}");
				}
			}

			if ( scaleChannels.Count() < 1 )
			{
				if (animnode.Parent != null && animnode.Parent.UseScale().IsAnimated)
				{
					CurveBuilder<Vector3> scaleCurve = animnode.UseScale(animName);
					CurveBuilder<Vector3> parentScaleTrack = animnode.Parent.UseScale(animName);
					float time = 0.0f;
					Vector3 parentScaleAtThisTime = parentScaleTrack.GetPoint(time);

					Vector3 diff = Vector3.One - parentScaleAtThisTime;

					scaleCurve.SetPoint(time, Vector3.One - diff);

					Console.WriteLine($"{animName} Calculated DUMMY local scale for scale keyframe: {Vector3.One - diff}");
				}
			}


			//Add the children's animation
			for (int idx = 0; idx < animJoint.Children.Length; idx++)
			{
				AddDataForJoint(name, animJoint.Children[idx], animnode.VisualChildren[idx]);
			}
		}

		static bool ValidateJointNames(SharedJointAnim root, Joint otherRoot)
		{
			if (root.Name != otherRoot.Name)
			{
				Console.WriteLine($"PASS: Animation joint {root.Name} == Skeleton joint {otherRoot.Name}");
			}
			else
			{
				Console.WriteLine($"FAIL: Animation joint {root.Name} != Skeleton joint {otherRoot.Name}");
				return false;
			}

			for (int i = 0; i < root.Children.Length; i++)
			{
				SharedJointAnim myChild = root.Children[i];
				if (i >= otherRoot.Children.Length)
				{
					Console.WriteLine($"FAIL: {root.Name} child {myChild.Name} missing on skeleton.");
					return false;
				}

				bool ret = ValidateJointNames(myChild, otherRoot.Children[i]);
				if (!ret)
					return ret;
			}

			return true;
		}

		private static void ValidateAnimAgainstMesh(AnimToProcess anim, Mesh mesh)
		{
			if (anim.anim.Header.JointsCount != mesh.Skeletons[0].JointsCount)
				Console.WriteLine("Warning! Animation has {0} joints than the skeleton! This is probably bad!", anim.anim.Header.JointsCount > mesh.Skeletons[0].JointsCount ? "more" : "fewer");
			else
				Console.WriteLine("Passed sanity check 1");

			if (ValidateJointNames(anim.anim.RootJoint, mesh.Skeletons[0].RootJoint))
			{
				Console.WriteLine("Passed sanity check 2");
			}
		}


		public static void ProcessSkelAnim(AnimToProcess anim, NodeBuilder rootNode, Mesh? mesh = null)
		{
			if ( mesh != null )
				ValidateAnimAgainstMesh(anim, mesh);
			

			AddDataForJoint(anim.name, anim.anim.RootJoint, rootNode);
		}
	}
}
