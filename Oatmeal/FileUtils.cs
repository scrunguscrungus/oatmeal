﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PsychoPortal;

using SharpGLTF.Schema2;

namespace Oatmeal
{
	struct IdentifiedFile
	{
		public readonly ModelRoot? glTF { get; }
		public readonly Scene? plb { get; }
		public readonly PackPack? ppf { get; }
		public readonly AnimToProcess[]? jan { get; }

		public IdentifiedFile(ModelRoot a)
		{
			glTF = a;
			plb = null;
			ppf = null;
			jan = null;
		}
		public IdentifiedFile(Scene a)
		{
			glTF = null;
			plb = a;
			ppf = null;
			jan = null;
		}
		public IdentifiedFile(PackPack a)
		{
			glTF = null;
			plb = null;
			ppf = a;
			jan = null;
		}
		public IdentifiedFile(AnimToProcess[] a)
		{
			glTF = null;
			plb = null;
			ppf = null;
			jan = a;
		}
	}
	internal static class FileUtils
	{
		public static IdentifiedFile? IdentifyFile(string path)
		{
			if ( Directory.Exists(path) )
			{
				//This is a directory - try to load animations from it
				List<AnimToProcess> anims = new List<AnimToProcess>();

				foreach ( string file in Directory.EnumerateFiles(path, "*", SearchOption.TopDirectoryOnly) )
				{
					try
					{
						SharedSkelAnim anim = Binary.ReadFromFile<SharedSkelAnim>(file, settings: Program.settings);
						AnimToProcess atp = new AnimToProcess()
						{
							name = Path.GetFileNameWithoutExtension(file),
							anim = anim
						};
						anims.Add(atp);
					}
					catch {
						Console.WriteLine($"{Path.GetFileName(file)} wasn't an animation!");
					}
				}

				return new IdentifiedFile(anims.ToArray());
			}

			if (!File.Exists(path))
				return null;

			try
			{
				SharedSkelAnim anim = Binary.ReadFromFile<SharedSkelAnim>(path, settings: Program.settings);
				return new IdentifiedFile(new AnimToProcess[] { new AnimToProcess() { name = Path.GetFileNameWithoutExtension(path), anim = anim } });
			} catch { }

			try
			{
				ModelRoot mr = ModelRoot.Load(path);
				return new IdentifiedFile(mr);
			} catch { }

			try
			{
				Scene scn = Binary.ReadFromFile<Scene>(path, settings: Program.settings);
				return new IdentifiedFile(scn);
			} catch { }

			//PPF load is potentially the slowest due to PPF size, so it goes last.
			try
			{
				PackPack ppf = Binary.ReadFromFile<PackPack>(path, settings: Program.settings);
				return new IdentifiedFile(ppf);
			} catch { }

			return null; //None of our handlers could load this file.
		}
	}
}
