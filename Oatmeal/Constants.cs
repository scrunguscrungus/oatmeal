﻿//GLOBAL USINGS//
global using Mesh = PsychoPortal.Mesh;
global using Scene = PsychoPortal.Scene;
global using VERTEX = SharpGLTF.Geometry.VertexTypes.VertexPositionNormal;
global using glScene = SharpGLTF.Schema2.Scene;

using PsychoPortal;
////

namespace Oatmeal.Constants
{
	internal class FixedValues
	{
		internal const float LiPoToBlender = 0.01f;
		internal const float BlenderToLiPo = 100f;
 
		internal const float FixedBounds = 32768f;
		internal static readonly Box3 FixedBox = new Box3(new Vec3(-FixedBounds, -FixedBounds, -FixedBounds), new Vec3(FixedBounds, FixedBounds, FixedBounds));

		internal static readonly Vec3 vecZero = new Vec3(0, 0, 0);
	}

	internal class Switches
	{
		internal const bool bEnableSkinning = true;
		internal const bool bEnableLightmapping = false;
	}

	internal class Strings
	{
		internal const string outFileFmt = "{0}_{1}_{2}";
	}
}
