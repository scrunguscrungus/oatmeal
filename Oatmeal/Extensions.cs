﻿using System.Numerics;

using PsychoPortal;

namespace Oatmeal
{

	/// <summary>
	/// Extensions dealing with those awful things people call quaternions.
	/// </summary>
	public static class QuatExtensions
	{
		/// <summary>
		/// Converts a LiPo Quat to a Numerics Quaternion
		/// </summary>
		public static Quaternion ToQuaternion(this Quat q)
		{
			return new Quaternion(q.X, q.Y, q.Z, q.W);
		}

		/// <summary>
		/// Converts a Numerics Quaternion to a LiPo Quat
		/// </summary>
		public static Quat ToQuat(this Quaternion quaternion)
		{
			return new Quat(quaternion.X, quaternion.Y, quaternion.Z, quaternion.W);
		}
	}

	/// <summary>
	/// Vec2 extensions
	/// </summary>
	public static class Vec2Extensions
	{	
		/// <summary>
		/// Convert from Vector2 to Vec2
		/// </summary>
		public static Vector2 ToVector2(this Vec2 vec2) => new Vector2(vec2.X, vec2.Y);
		/// <summary>
		/// Convert from Vec2 to Vector2
		/// </summary>
		public static Vec2 ToVec2(this Vector2 vec2) => new Vec2(vec2.X, vec2.Y);

	}

	/// <summary>
	/// Vec3 extensions
	/// </summary>
	public static class Vec3Extensions
	{
		/// <summary>
		/// Convert from Vec3 to Vector3
		/// </summary>
		public static Vector3 ToVector3(this Vec3 vec3) => new Vector3(vec3.X, vec3.Y, vec3.Z);

		/// <summary>
		/// Convert from Vector3 to Vec3
		/// </summary>
		public static Vec3 ToVec3(this Vector3 vec3) => new Vec3(vec3.X, vec3.Y, vec3.Z);

		/// <summary>
		/// Inverts a Vec3
		/// </summary>
		public static Vec3 Invert(this Vec3 v) => v * -1;

		//Except this one, this one will stay
		public static Vec3 Subtract(this Vec3 v, Vector3 other)
		{
			return v-other.ToVec3();
		}
	}

	public static class Vec4Extensions
	{
		public static Vector4 ToVector4(this Vec4 v) => new Vector4(v.X, v.Y, v.Z, v.W);
		public static Vec4 ToVec4(this Vector4 v) => new Vec4(v.X, v.Y, v.Z, v.W);
	}

	/// <summary>
	/// Horrendous, despicable extensions related to converting rotations.
	/// Which... don't really work.
	/// </summary>
	public static class RotationExtensions
	{
		/// <summary>
		/// Alias for SafeToVec3
		/// </summary>
		public static Vec3 ToVec3(this Quaternion q) => q.ToQuat().ToEuler();

		/// <summary>
		/// This function will convert a Numerics Quaternion to a Vec3
		/// It mitigates gimbal lock by treating the rotation as YZX and zeroing out Z if
		/// Y is at 90 or -90 degrees.
		/// </summary>
		public static Vec3 SafeToVec3(this Quaternion q)
		{
			Vec3 angles = new Vec3();

			double sinp = 2 * (q.W * q.Y - q.Z * q.X);
			if (Math.Abs(sinp) >= 0.98)
			{
				//Gimbal lock for ZYX
				angles.Y = (float)Math.CopySign(Math.PI / 2, sinp) * MathHelpers.Rad2Deg;

				angles.X = MathHelpers.ArcTan(2 * q.X * q.W - 2 * q.Y * q.Z, 1 - 2 * (q.X * q.X) - 2 * (q.Z * q.Z));
				angles.Z = 0;
			}
			else
			{
				//We good
				angles.Y = (float)Math.Asin(sinp) * MathHelpers.Rad2Deg;

				double sinr_cosp = 2 * (q.W * q.X + q.Y * q.Z);
				double cosr_cosp = 1 - 2 * (q.X * q.X + q.Y * q.Y);
				angles.X = MathHelpers.ArcTan((float)sinr_cosp, (float)cosr_cosp);

				double siny_cosp = 2 * (q.W * q.Z + q.X * q.Y);
				double cosy_cosp = 1 - 2 * (q.Y * q.Y + q.Z * q.Z);
				angles.Z = MathHelpers.ArcTan((float)siny_cosp, (float)cosy_cosp);
			}

			return angles;
		}

		/// <summary>
		/// This is meant to convert a Vec3 to a Quaternion.
		/// The Vector3 will automatically be converted to radians during the conversion, so don't worry about it.
		/// </summary>
		public static Quaternion ToQuaternion(this Vec3 v)
		{
			Quat q = new Quat();
			q.FromEuler(v);
			return q.ToQuaternion();
		}
	}

	/// <summary>
	/// Miscellaneous extensions
	/// </summary>
	public static class Extensions
	{

		public static Joint? GetJoint(this Skeleton s, int jointID)
		{
			if (jointID == 0)
				return s.RootJoint;

			Joint? FindJoint(Joint j, int jointID)
			{
				if (j.ID.JointIndex == jointID)
					return j;

				foreach( Joint c in j.Children )
				{
					Joint? result = FindJoint(c, jointID);
					if (result != null)
						return result;
				}

				return null;
			}


			return FindJoint(s.RootJoint, jointID);
		}

		//Adds an element to a list and returns its index
		public static int AddGetIndex<T>(this List<T> list, T item)
		{
			list.Add(item);
			return list.Count - 1;
		}
		
	}
}
